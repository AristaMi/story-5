from django import forms
from .models import ModelJadwal
from django.forms import ModelForm, TextInput, SelectDateWidget

class FormJadwal(forms.ModelForm):
	class Meta:
		model = ModelJadwal
		fields = [
			'Day',
			'Date',
			'Time',
			'Activity',
			'Place',
			'Category'
		]
		widgets = {
			'Date' : TextInput(attrs={'placeholder': 'YYY-MM-DD'}),
			'Date' : SelectDateWidget(),
			'Time' : TextInput(attrs={'placeholder': 'HH:MM'}),
			'Activity' : TextInput(attrs={'placeholder': 'Mengerjakan tugas, main, dsb.'})
			'Place' : TextInput(attrs={'placeholder': 'Aula, Rumah, dsb.'})
		}