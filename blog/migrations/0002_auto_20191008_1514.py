# Generated by Django 2.2.5 on 2019-10-08 08:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ModelJadwal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Day', models.CharField(choices=[('Monday', 'Monday'), ('Tuesday', 'Tuesday'), ('Wednesday', 'Wednesday'), ('Thursday', 'Thursday'), ('Friday', 'Friday'), ('Saturday', 'Saturday'), ('Sunday', 'Sunday')], default='Senin', max_length=10)),
                ('Date', models.DateField()),
                ('Time', models.TimeField()),
                ('Activity', models.CharField(max_length=50)),
                ('Place', models.CharField(max_length=50)),
                ('Category', models.CharField(choices=[('Class', 'Class'), ('Study', 'Study'), ('Meeting', 'Meeting'), ('Hangout', 'Hangout'), ('F5', 'F5')], default='Kelas', max_length=10)),
            ],
        ),
        migrations.DeleteModel(
            name='Post',
        ),
    ]
