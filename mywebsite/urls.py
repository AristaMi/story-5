from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    #path('admin/', admin.site.urls),
    path('blog', include('blog.urls')),
    # path('jadwal', include(('blog.urls', 'blog'), namespace="kegiatan")),
    # path('form/', include('blog.urls')),
    path('', views.index),
    path('schedule', include('form.urls')),
]
