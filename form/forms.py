from django import forms
from .models import ModelJadwal
from django.forms import ModelForm, TextInput, SelectDateWidget

# Form menggunakan ModelForm
class FormJadwal(forms.ModelForm):
	class Meta:
		model = ModelJadwal
		fields = [
			'Day',
			'Date',
			'Time',
			'Activity',
			'Place',
			'Category',
		]
		widgets = {
			'Date': TextInput(attrs={'placeholder': 'YYYY-MM-DD'}),
			'Date': SelectDateWidget(),
			'Time': TextInput(attrs={'placeholder': 'HH:MM'}),
			'Activity': TextInput(),
			'Place': TextInput(),
		}