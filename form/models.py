from django.db import models
from datetime import datetime
from django.core.exceptions import ValidationError

class ModelJadwal(models.Model):
	HARI = (
		('Monday', 'Monday'),
		('Tuesday', 'Tuesday'),
		('Wednesday', 'Wednesday'),
		('Thursday', 'Thursday'),
		('Friday', 'Friday'),
		('Saturday', 'Saturday'),
		('Sunday', 'Sunday'),
	)

	KATEGORI = (
		('Penting', 'Penting'),
		('Lumayan Penting', 'Lumayan Penting'),
		('Tidak Penting', 'Tidak Penting'),
	)

	Day 		= models.CharField(max_length=10, choices=HARI)
	Date		= models.DateField()
	Time		= models.TimeField()
	Activity	= models.CharField(max_length=50)
	Place		= models.CharField(max_length=50)
	Category	= models.CharField(max_length=10, choices=KATEGORI)
	
