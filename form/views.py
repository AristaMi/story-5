from django.shortcuts import render, redirect

from .forms import FormJadwal
from .models import ModelJadwal


def index(request):
	modelJadwal = ModelJadwal.objects.all()
	context = {
		'hasilJadwal' : modelJadwal,
	}

	return render(request, 'Schedule.html', context)

def create(request):
	formJadwal = FormJadwal(request.POST or None)	# request.POST buat validasi

	# Masukin dari form ke database
	if request.method == 'POST':
		if formJadwal.is_valid():	# Kalau input valid
			formJadwal.save()
			
			return redirect('/schedule')	# Abis ngisi form redirect ke halaman schedule

	context = {
		'formJadwal' : formJadwal,
	}

	return render(request, 'Form.html', context)

def delete(request, delete_id):
	ModelJadwal.objects.filter(id=delete_id).delete()
	return redirect('/schedule')