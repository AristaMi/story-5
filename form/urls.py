from django.conf.urls import url
from . import views

urlpatterns = [
	url('form', views.create,  name='create'),
	url('delete/(?P<delete_id>[0-9]+)', views.delete),
	url('', views.index, name='index'),	
]